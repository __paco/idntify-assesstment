![Idntify Logo](https://cdn.idntify.io/idntify-logo.png)

# Technical Assestment

Idntify has a group of marketing and data science experts who came up with a really great idea, to launch an anti fraud solution based on customers behavior while visiting e-commerce sites. Your task is to gather customers information using a script that will be embedded on these sites.

Idntify wants you to gather all relevant information that could potentially be used to create a Machine Learning algorithm to decide whether a transaction (any online purchase) is fraud or not. 

It is up to you to decide what data you will gather, just keep in mind that:
- The solution will be embedded on e-commerce sites
- The solution must be script based
- The solution has to NOT affect user navigation in any way or conflict with the e-commerce libraries, frameworks, etc.
- The solution must gather as much information as possible

### Your solution must have the following deliverables
- A diagram of a proposed cloud based architecture. It MUST be scalable and be able to handle millions of requests
- A snippet/script or anything of your choice that the e-commerce will implement to "silently" gather the customer's information.
- An API (that the API will communicate with)
- A diagram/schema/ERD of your database.
- Documentation related all your deliverables (instructions on how to do the integration from the client side, meaning the e-commerce sites, documentation on the API, the architecture, the db schema, etc.) It must not be exhaustive, just what you consider important enough.
- A client (a dashboard where Idntify will be able to see all gathered information from customers)

---
## Dashboard

The dashboard should:

- Be a simple dashboard where Idntify can view the collected data.
- There are no more guidelines

---
## API

The API should:

- There are actually no guidelines for how you will construct your API. You have to register as many methods as you need to make it work.

---

#### Notes
- You have to gather information from the end users, what this assessment is about is to see really how much data you can gather from the end user. There are no guidelines really, and the more data you gather, the better.
- Your solution **MUST** be easy to implement on **ANY** e-commerce
- Needless to say, you must build the data schema. It must be clear, easy to understand and have a good structure.
- Also, needless to say. This requires you to **IMPLEMENT** a script or anything for the client (e-commerce) and the easier the integration for the e-commerce, the better.

---

#### The whole solution should:
- Have POSTGRES as the database system (no, you cannot use Mongo or anything, it is a constraint)
- Be portable, can be built, executed, replicated, etc in any system (include description/steps on how to launch the project)
- Be written in React for the frontend, and NodeJS + Express / Hapi for the backend (if you decide to use another framework, be sure to justify it on the documentation/deliverables)
- Be easy to grow with new functionality.
- Don't include binaries, .sh files, etc. Use a dependency management tool (npm or yarn).

---
#### Bonus Points For:
- Tests, the more coverage the better
- Comments, a good README, instructions, etc.
- Docker images / CI, docker-compose, pipelines, etc.
- Commit messages (include .git in zip)
- Clear scalability

---
#### Notes:
- You might think the assessment is too long, and you may be right, so, if you don't have enough time for everything, make sure you deliver something. 
- We are more interested in getting to know your coding style and other technical skills, so, we will focus on evaluating the deliverables in the order you can find them on the **"Your solution must have the following deliverables"** section. Having this in mind, you should focus on creating them following that order.
- If you have any questions, feel free to contact paco@idntify.io, also, any suggestions are welcome


Good luck

